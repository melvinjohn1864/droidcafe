



// Group Members

// 1. Melvin John - C0742327
// 2. Akhil Somaraj - C0721763





package com.example.droidcafewsettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.robolectric.Shadows.shadowOf;

import org.apache.tools.ant.util.WeakishReference;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;



// Group Members

// 1. Melvin John - C0742327
// 2. Akhil Somaraj - C0721763



@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;
    private OrderActivity orderActivity;;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);
    }

    @Test
    public void orderFrozenYogurtTest() throws Exception {
        //1. Getting the imageview yogurt
        ImageView view = (ImageView) activity.findViewById(R.id.froyo);

        //2. Check that imageview exists in the activity
        assertNotNull(view);

        //3. Performing click on imageview
        view.performClick();

        //4. Check that latest shadow toast is equal to the expected toast
        assertThat(ShadowToast.getTextOfLatestToast(), equalTo("You ordered a FroYo.") );
    }

    @Test
    public void cartPageRememberUserInfo() throws Exception {
        //G1. Get into the order activity
        orderActivity = Robolectric.buildActivity(OrderActivity.class).create().get();

        //2. Get all the user infos saved in cart.
        EditText name = (EditText) orderActivity.findViewById(R.id.name_text);
        EditText address = (EditText) orderActivity.findViewById(R.id.address_text);
        EditText phone = (EditText) orderActivity.findViewById(R.id.phone_text);
        EditText note = (EditText) orderActivity.findViewById(R.id.note_text);

        //3. Check that the edittext is not equal to null
        assertNotNull(name);
        assertNotNull(address);
        assertNotNull(phone);
        assertNotNull(note);

        //4. Insert values into the edittexts
        name.setText("Melvin");
        address.setText("60 sandrift square");
        phone.setText("123456789");
        note.setText("One Day Delivery");

        //5. Getting the customerinfo save button
        Button saveButton = (Button) orderActivity.findViewById(R.id.saveButton);

        //6. Performing click on save button
        saveButton.performClick();

        //7. Go back to the previous activity and come back.
        //orderActivity.finish();
        orderActivity.onBackPressed();


        //8. Check that user info is saved
        assertThat(name.getText().toString(), equalTo("Melvin") );
        assertThat(address.getText().toString(), equalTo("60 sandrift square") );
        assertThat(phone.getText().toString(), equalTo("123456789") );
        assertThat(note.getText().toString(), equalTo("One Day Delivery") );

    }




}
